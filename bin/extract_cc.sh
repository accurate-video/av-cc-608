#!/bin/sh

echo "Processing $1"
ccextractor -out=srt -12 "$1"

# Status code 10 if no files found
if [ $? = 10 ] ; then
  echo "No cc found"
  exit 0
fi
