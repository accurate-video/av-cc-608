#!/bin/sh -e

# Example
# 14153_mp4-noaudio.mp4 (input asset file)
# 14153_mp4-noaudio_1.srt (output srt file 1)
# 14153_mp4-noaudio_2.srt (output srt file 2)

filename=$(basename -- "$1")
filename="${filename%.*}"

# Run for both CC files
for i in 1 2
do
  CC_FILE=$filename"_"$i".srt"

  if [ -s "$CC_FILE" ] ; then
    echo "Ingesting $CC_FILE"

    rm -f timespans.json

    # Convert CC file to timespans.json
    /usr/local/bin/subtitles/vttreader.py "$CC_FILE" timespans.json

    # Set language
    /usr/local/bin/subtitles/sub_language.py timespans.json en-US timespans.json

    # Ingest to asset
    curl -X POST "${AP_ADAPTER_URL_MP_REST_URL}/asset/$2/timespan/bulk"  -u "${AP_ADAPTER_USERNAME}:${AP_ADAPTER_PASSWORD}"  -H 'Content-Type: application/json' -d @timespans.json
  fi
done






