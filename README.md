# Extraction of 608 closed captions in Accurate.Video

This repository contains custom job templates for extracting 608 Closed Captions during ingest in Accurate.Video.   
For an in-depth overview of custom job templates in AV, refer to this repo: https://gitlab.com/accurate-player/av-ffmpeg-blackframe

## CCExtractor

CCExtractor is a free, open source tool that analyzes video files and produces independent subtitle files from the 
closed captions data. CCExtractor is portable, small, and very fast. It's bundled in the Accurate.Video job runner.

https://ccextractor.org/

## 608

More details on EIA-608 captions can be found here:

https://en.wikipedia.org/wiki/EIA-608

## Scripts

* Extracting captions - `bin/extract_cc.sh <input file>`
* Ingesting captions - `bin/ingest_cc.sh <input file> <asset id>`

## Job template

See `partials/video_ingest.j2.json`. Snippet:

```json
{
  "type": "SHELL",
  "metadata": [
    {
      "key": "command",
      "value": "/opt/dynamic-settings/runner-script/extract_cc.sh"
    },
    {
      "key": "command",
      "value": "${target_path_export}"
    }
  ]
},
{
  "type": "SHELL",
  "metadata": [
    {
      "key": "command",
      "value": "/opt/dynamic-settings/runner-script/ingest_cc.sh"
    },
    {
      "key": "command",
      "value": "${target_path_export}"
    },
    {
      "key": "command",
      "value": "{{metadata.target_asset_id}}"
    }
  ]
},
```

## Setting language

Language isn't extracted from CCExtractor, but can be set optionally.

```bash
# Set language
/usr/local/bin/subtitles/sub_language.py timespans.json en-US timespans.json
```

## Uploading scripts & templates

Use the following scripts for uploading template & scripts to runner:

https://gitlab.com/accurate-player/av-ffmpeg-blackframe/-/blob/master/bin/upload_template.py  
https://gitlab.com/accurate-player/av-ffmpeg-blackframe/-/blob/master/bin/upload_script.py

```
./upload_template.py 'partials/video_ingest' '../partials/video_ingest.j2.json' 'http://...' 'runner' 'xxx'
./upload_script.py 'extract_cc.sh' 'extract_cc.sh' 'http://...' 'runner' 'xxx'
./upload_script.py 'ingest_cc.sh' 'ingest_cc.sh' 'http://...' 'runner' 'xxx'
```
